

  class Animal {
      constructor(name){
          this.name = name;
      }
      getName() {
          return name
      }
  }

  class Dog extends Animal {
      constructor(name){
          super(name);
      }
      bark(){
          console.log( `Dog ${this.name} is barking` );
      }  
  };

  //*** RESULT ***
  let dog = new Dog ("Aban");
  console.log(dog.getName() === 'Aban');
  dog.bark();

 