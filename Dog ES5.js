

  var Animal = function (param) {
      this.getName = function() {
          this.name = param;
          return this.name
          //console.log( `my name is ${this.name}` )
    }
  };

  var Dog = function () {
      Animal.apply(this, arguments);
      this.bark = function (){        
          console.log( `Dog ${this.name} is barking` );
      };  
  };

  Dog.prototype = Object.create(Animal.prototype)

    //*** RESULT ***
    let dog = new Dog ("Aban");
    console.log(dog.getName() === 'Aban');
    dog.bark();
  